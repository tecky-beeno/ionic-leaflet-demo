import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet'
import React, { useState, useEffect } from 'react'
import 'leaflet/dist/leaflet.css'
import markerIconUrl from 'leaflet/dist/images/marker-icon.png'
import { Icon } from 'leaflet'

let markerIcon = new Icon({ iconUrl: markerIconUrl })

export function MapOuter() {
  console.log(Date.now(), 'render outer')
  return (
    <MapContainer
      center={[51.505, -0.09]}
      zoom={13}
      scrollWheelZoom={false}
      style={{
        // width: '400px',
        // height: '400px',
        width: '100%',
        height: '78vh',
      }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[51.505, -0.09]} icon={markerIcon}>
        <Popup>
          A pretty CSS3 popup. <br /> Easily customizable.
        </Popup>
      </Marker>
      <MapRefresher />
    </MapContainer>
  )
}
export function MapRefresher() {
  const [interval, setInterval] = useState(37)
  let map = useMap()
  useEffect(() => {
    let timer = setTimeout(() => {
      console.log(Date.now(),'invalidateSize')
      map.invalidateSize()
      setInterval(interval => interval * 1.5)
    }, interval)
    return () => clearTimeout(timer)
  }, [map, interval])
  return <></>
}

export function Map() {
  return <MapOuter />
}

export function MapMixed() {
  console.log(Date.now(), 'render')
  let map = useMap()
  console.log(Date.now(), map)
  return (
    <MapContainer
      center={[51.505, -0.09]}
      zoom={13}
      scrollWheelZoom={false}
      style={{
        // width: '400px',
        // height: '400px',
        width: '100%',
        height: '78vh',
      }}
      whenReady={() => {
        console.log(Date.now(), 'ready')
      }}
      whenCreated={map => {
        // console.log(Date.now(), 'created')
        // function monitor(type: string) {
        //   map.once(type, () => console.log(Date.now(), type))
        // }
        // monitor('load')
        // monitor('loading')
        // monitor('update')
        // monitor('error')
        // monitor('tileloadstart')
        // monitor('tileload')
        // monitor('tileerror')
        // setTimeout(() => {
        //   console.log(Date.now(), 'invalidateSize')
        //   map.invalidateSize()
        // }, 1000)
      }}
    >
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[51.505, -0.09]}>
        <Popup>
          A pretty CSS3 popup. <br /> Easily customizable.
        </Popup>
      </Marker>
    </MapContainer>
  )
}
export default Map
