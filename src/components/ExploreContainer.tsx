import './ExploreContainer.css';
import Map from './Map';

interface ContainerProps { }

const ExploreContainer: React.FC<ContainerProps> = () => {
  if('dev'){
    return <Map/>
  }
  return (
    <div className="container">
      <strong>Ready to create an app?</strong>
      <p>Start with Ionic <a target="_blank" rel="noopener noreferrer" href="https://ionicframework.com/docs/components">UI Components</a></p>
      <Map/>
    </div>
  );
};

export default ExploreContainer;
